
# Getting Started:
---------------

Run WaveSignalDisplay.py (or link WaveSignalDisplay.desktop to the desktop).  Note that a sensor hat must be installed on the raspberry pi unit.

# Prerequsities:
-------------

    * dateutil
    * pandas

## For Rasbian-based systems:

`sudo apt install python-dateutil python-pandas`

or for Python 3

`sudo apt install python3-dateutil python3-pandas`

