#!/usr/bin/env python

# import libraries
try:
    from SenseHatRGBOutput import SenseHatRGBOutput
except:
    from PlotRGBOutput import PlotRGBOutput
from WaveSignal import WaveSignal
from StationLocator import StationLocator
from threading import Thread
import datetime as dt

class WaveSignalDisplay:

    def __init__(self,  \
                rgb_output, \
                ndbc_id, \
                north_hue = 0.0, \
                update_delay = dt.timedelta(minutes = 15), \
                ):

        # save a few of the input arguments
        self.rgb_output = rgb_output
        # store the update delay for when we have internet
        self.internet_up_update_delay = update_delay
        # store the update delay for when internet is down
        self.internet_down_update_delay = dt.timedelta(seconds=15)
        # set the update delay
        self.update_delay = self.internet_up_update_delay

        # initialize NDBC interface
        self.wave_signal = WaveSignal(ndbc_id, north_hue = north_hue) 

    def run_update_loop(self):
        """ Runs the RGB update loop indefinitely """

        # enter a forever loop
        while True:
            # get the current date/time
            current_time = dt.datetime.now()

            # check if we should update the NDBC data
            if current_time - self.wave_signal.last_update_check >= self.update_delay:
                # print a status update
                print("""Updating NDBC Data...
        last check at {}
        most recent data from {}""".format(self.wave_signal.last_update_check,self.wave_signal.most_recent_obs_time))

                # spawn a new thread to pull down new data from NDBC
                # (do this in a thread so as not to interrupt the color cycling)
                Thread(target=self.wave_signal.update_buoy_data,args=()).start()


            if not self.wave_signal.has_good_data:
                # set the update delay to a higher frequency
                self.update_delay = self.internet_down_update_delay
            else:
                # set the update delay to a lower frequency
                self.update_delay = self.internet_up_update_delay


            # get the current color from the NDBC WaveSignal object
            rgb, signal = self.wave_signal.get_wave_sample()

            # set the color of the output device
            self.rgb_output.set_color(rgb,signal)
                
    def __del__(self):

            # if the object goes out of scope or is deleted, turn off gracefully
            # turn off the LCD
            self.rgb_output.clear()

#*******************************************************************************
#*******************************************************************************
#******* This is where the program execution starts ****************************
#*******************************************************************************
#*******************************************************************************

try:
    # set the buoy to the closest station
    ndbc_id = StationLocator().get_closest_buoy_id()
except:
    # manually set the buoy ID to a buoy near SB if the geolocation lookup fails
    ndbc_id = 46054 

north_hue = 0.8 # the hue of waves coming from true north
try:
    rgb_output = SenseHatRGBOutput() #initialize the connection to the RGB display
except:
    rgb_output = PlotRGBOutput() #initialize the connection to the RGB plotter
update_delay = dt.timedelta(minutes=15) # time between NDBC inquiries

wave_signal_display = WaveSignalDisplay(ndbc_id = ndbc_id, \
                                        rgb_output = rgb_output, \
                                        north_hue = north_hue, \
                                        update_delay = dt.timedelta(minutes=15)\
                                        )

try:
    # print a message so the user knows how to kill the program cleanly
    print("Starting...hit <ctrl-c> to exit")

    # enter the program loop
    wave_signal_display.run_update_loop()

except KeyboardInterrupt:
    # if we get a keyboard interrupt, kill the wave_signal_display object
    del(wave_signal_display)
