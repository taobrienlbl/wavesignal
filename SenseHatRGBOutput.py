from RGBOutput import RGBOutput
from sense_hat import SenseHat
import struct

class SenseHatRGBOutput(RGBOutput):

    def __init__(self):
        # initialize the sensor hat device
        self.sense_hat = SenseHat()

    def set_color(self, rgb, signal):
        """ Sets the rgb values of the device. 

        input:
        ------
            
            rgb     : (R,G,B) a tuple of values between 0 and 1

            signal  : the amplitude of the output (ignored)

        
        """

        # convert the RGB 0-1 values to 0-255
        r = min(max(int(rgb[0]*255),0),255)
        g = min(max(int(rgb[1]*255),0),255)
        b = min(max(int(rgb[2]*255),0),255)

        # encode RGB for sending to the sensor_hat device
        # taken from the python-sense-hat/sense_hat/sense_hat.py
        # function _pack_bin
        r = (r >> 3) & 0x1F
        g = (g >> 2) & 0x3F
        b = (b >> 3) & 0x1F
        bits16 = (r << 11) + (g << 5) + b
        # pack the bits into a hex structure; repeat 64 times
        # (once for each pixel)
        rgb_hex = struct.pack('H', bits16)
        rgb_hex_8x8 = b''.join(64*[rgb_hex])

        # Write the color to all pixels of the sensor hat RGB array
        with open(self.sense_hat._fb_device, 'wb') as frgb:
            frgb.write(rgb_hex_8x8)

        return

    def clear(self):
        """ Turns off the RGB display. """

        self.sense_hat.clear()

