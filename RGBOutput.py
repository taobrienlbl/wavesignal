class RGBOutput:
    """ A dummy class for RGB output; defines the programming API
        for a generic RGB output device.  This is meant to be subclassed."""

    def set_color(self,rgb, signal):
        """ Sets the rgb values of the device. 

        input:
        ------
            
            rgb     : (R,G,B) a tuple of values between 0 and 1

            signal  : the amplitude of the output

        
        """
        pass

    def clear(self):
        """ Turns off the RGB display. """
        pass


