from RGBOutput import RGBOutput
import matplotlib as mpl
mpl.use('TKagg')
import pylab as PP
import numpy as np
import struct

class PlotRGBOutput(RGBOutput):

    def __init__(self):
        # initialize the plot
        fig,ax = PP.subplots(figsize=(6,4))
        self.fig = fig
        self.ax = ax

        #ax.set_frame_on(False)
        #ax.axes.get_yaxis().set_visible(False)
        ax.axes.get_xaxis().set_visible(False)

        tmax = 60
        ax.set_xlim([0,tmax])
        ax.set_ylim([0,1])

        ax.set_xlabel("Relative Time [s]")
        ax.set_ylabel("Signal Height [unitless]")

        self.num_cached_points = 500
        self.time_values = np.linspace(0,tmax,self.num_cached_points)
        self.wave_values = np.zeros(self.num_cached_points)
        self.colors = np.zeros([self.num_cached_points,3])
        scatter1 = ax.scatter(self.time_values, \
                              self.wave_values, \
                              c = self.colors, \
                              linewidth = 0.0, \
                              edgecolor = (0,0,0,0),\
                              )
        self.scatter = scatter1

        self.background = self.fig.canvas.copy_from_bbox(self.ax.bbox)

        PP.show(block=False)



    def set_color(self, rgb, signal):
        """ Sets the rgb values of the device. 

        input:
        ------
            
            rgb     : (R,G,B) a tuple of values between 0 and 1

            signal  : the amplitude of the output

        
        """

        # force the rgb values to be between 0 and 1
        rgb = np.clip(rgb,0,1)

        np.roll(self.colors, 1)
        np.roll(self.wave_values, 1)
        #np.roll(self.time_values, -1)

        self.colors[-1,:] = rgb
        self.wave_values[-1] = signal

        # update scatter point positions
        self.scatter.set_offsets(np.array([self.time_values,self.wave_values]))

        # update scatter point colors
        self.scatter.set_color(self.colors)

        self.fig.canvas.restore_region(self.background)

        self.ax.draw_artist(self.scatter)

        self.fig.canvas.blit(self.ax.bbox)

        self.fig.canvas.flush_events()

        return

    def clear(self):
        """ Turns off the RGB display. """

        pass

