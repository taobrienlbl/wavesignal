import datetime as dt
from dateutil import tz
import colorsys
import pandas as pd
import math
import warnings

class WaveSignal:
    """ A class for downloading, parsing, and visualizing NDBC buoy data. """
       
    
    def __init__(self, \
                 buoy_id, \
                 ndbc_realtime_data_template = "https://www.ndbc.noaa.gov/data/realtime2/{}.txt", \
                 north_hue = 0, \
                 max_wave_height = 4.0):
        """
            input:
            ------

                buoy_id                     : a valid NDBC buoy ID

                ndbc_realtime_data_template : the URL template for NDBC data

                north_hue                   : the hue to assign to waves coming
                                              from true north

                max_wave_height             : the maximum wave height (modulates
                                              the amplitude of the output
                                              sine wave)
        """
        
        # store input arguments
        self.ndbc_realtime_url = ndbc_realtime_data_template.format(buoy_id)

        # set the hue of waves coming from due north
        self.set_north_hue(north_hue)
       
        # inialize some class variables
        self.data_table = None
        self.most_recent_obs_time = None
        self.wave_height = None
        self.wave_direction = None
        self.wave_period = None
        self.last_update_check = None
        self.has_good_data = False

        # store the max wave height scaling
        self.set_max_waveheight(max_wave_height)

        # set the buoy and get the most recent buoy data
        self.set_buoy_id(buoy_id)

        # set the time=0 counter for the get_wave_sample() routine
        self.time0 = dt.datetime.now()

    def set_buoy_id(self,buoy_id, auto_update = True):
        """ Set the buoy ID """

        # store the new buoy ID
        self.buoy_id = buoy_id

        if auto_update:
            self.update_buoy_data()

    def set_max_waveheight(self,max_wave_height):
        """ Sets the maximum waveheight (scales brightness). """
        # set the new maximum wave height (don't update if the value is bad)
        try:
            new_max = float(max_wave_height)
        except:
            new_max = self.max_wave_height

        self.max_wave_height = new_max

    def set_north_hue(self, north_hue):
        """ Sets the hue of waves coming from true north. 
            
            input:
            -----

                north_hue   : a value between 0 and 1

            If north_hue is beyond these bounds, or is otherwise invalid,
            the north_hue will default to 0.0.
        
        """

        try:
            # attempt to convert hue to a floating point number
            self.north_hue = float(north_hue)
            # flag an error if hue is not between 0 and 1
            if self.north_hue < 0 or self.north_hue > 1:
                raise 
        except:
            self.north_hue = 0.0
 
        
    def update_buoy_data(self):
        """ Updates the buoy data table """
        
        # set the update time
        self.last_update_check = dt.datetime.now()

        try:
            # download and parse the data table
            data_table = pd.read_fwf(self.ndbc_realtime_url,header=[0,1])
        except:
            # issue a warning
            warnings.warn("Data download/parse failed.")

            # if accessing the URL  didn't work, simply keep using the 
            # same data table
            data_table = self.data_table
        
        # store the data table
        self.data_table = data_table
        
        # if the data download/parse was successful, find the most recent
        # valid data
        if self.data_table is not None:
            # loop over rows to search for valid data
            for n in range(len(self.data_table)):
                try:
                    # parse the date from the table
                    year = int(self.data_table['#YY'].loc[n])
                    month = int(self.data_table['MM'].loc[n])
                    day = int(self.data_table['DD'].loc[n])
                    hour = int(self.data_table['hh'].loc[n])
                    minute = int(self.data_table['mm'].loc[n])

                    # set the most recent observation date and convert
                    # from the native UTC timezone to the timezone reported
                    # by the computer
                    self.most_recent_obs_time = dt.datetime(year,month,day,hour,minute,tzinfo = tz.tzutc()).astimezone(tz.tzlocal())
                except:
                    # flag that we have no observation
                    self.most_recent_obs_time = None

                try:
                    # extract the wave height, period, and direction from the 
                    # current table row
                    self.wave_height, self.wave_period, self.wave_direction = \
                        float(self.data_table[('WVHT', 'm')].loc[n]), \
                        float(self.data_table[('DPD', 'sec')].loc[n]), \
                        float(self.data_table[('MWD   PRES', 'degT   hPa')].loc[n].split()[0])
                except:
                    # if that fails (many rows have missing data, for example),
                    # flag that we don't yet have good data
                    self.wave_height, self.wave_period, self.wave_direction = None, None, None
                    self.has_good_data = False

                # set the list of variables needed for a complete observation
                critical_data_list = [\
                                      self.most_recent_obs_time, \
                                      self.wave_height, \
                                      self.wave_period, \
                                      self.wave_direction, \
                                     ]

                # check if we have all of the data needed; 
                if all([ item is not None for item in critical_data_list]):
                    # if valid data have been found, flag this and
                    # break out of the loop
                    self.has_good_data = True
                    break
                    
        # print feedback that the download was successful
        print("NDBC ID {} downloaded; obs. are from {}".format(self.buoy_id,self.most_recent_obs_time))
                    
    def get_color(self, \
                  saturation = 0.66666, \
                  value = 1.0):
        """ Returns an RGB color corresponding to wave direction 
        
            input:
            ------

                saturation  : the saturation value (0-1) for the color

                value       : the brightness of the color

            output:
            -------

                returns a red-green-blue tuple, where the hue of the color
                represents the wave direction.  
        
        """
        # only get a color if we have good data
        if self.has_good_data:
            # set the hue of the color
            hue = self.wave_direction/360 + self.north_hue
            # wrap the hue around if it is out-of-bounds
            if hue > 1:
                hue -= 1
            if hue < 0:
                hue += 1
            

            # conver the color to RGB
            rgb = colorsys.hsv_to_rgb(hue,saturation,value)
        else:
            rgb = (0,0,0)
        
        return rgb
        
    def get_seconds_elapsed(self):
        """ Gets the seconds elapsed, for the purposes of signal generation """
        # get and store the current time
        self.current_time = dt.datetime.now()
        return (self.time0 - self.current_time).total_seconds()
        
        
        
    def get_wave_sample(self, \
                        saturation = 0.6666666):
        """ Returns an RGB value that represents time along a sinusoidal wave """
        
        
        # if we have good data
        if self.has_good_data:
            # set and clip the amplitude to 1
            amplitude = min(self.wave_height / self.max_wave_height, 1.0)
            
            # create the signal (ranges from 0 to 1 maximum)
            signal = amplitude*math.sin(2*math.pi*self.get_seconds_elapsed()/self.wave_period) 

            # when the signal is close to 0, reset the time counter
            if abs(signal) <= 1e-5:
                # reset the time counter to avoid eventual overflow issues
                self.time0 = self.current_time
                
            # get the color; it's brightness will be proportional to the current sine-wave amplitude
            # that represents the wave pulse
            rgb = self.get_color(value=signal, saturation = saturation)
        else:
            rgb = (0,0,0)
            signal = 0
            
        return rgb, signal
        
        
