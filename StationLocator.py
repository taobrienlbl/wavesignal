import pandas as pd
import os
import math

def distance_on_earth_sphere(lat1, long1, lat2, long2, rearth = 6.371e6):
    """ Calculates the great-circle distance between two points on Earth.  
        Adapted from https://www.johndcook.com/blog/python_longitude_latitude/"""

    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
        
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
        
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
        
    # Compute spherical distance from spherical coordinates.
        
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
    
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
    
    # check if we accidently got the long distance
    if arc > math.pi:
        # correct the distance if so
        arc = 2*math.pi - arc
    
    # convert the arc length to distance
    distance = arc*rearth

    return distance


class StationLocator:
    """ Attempts to locate a nearest NDBC station based on geolocation look-up """

    def __init__(self, \
                 latlon = None, \
                 station_table = None, \
                 ):
        """ Helps sort stations based on distance from location. """
        
        self.latlon = latlon
        
        # if no lat/lon was provided, use a web API to estimate
        # the user's location based on IP address
        if self.latlon is None:
            import json
            try:
                from urllib2 import urlopen
            except ImportError:
                # import properly for python3
                from urllib.request import urlopen

            # get IP geolocation info from ipinfo.io
            url = 'http://ipinfo.io/json'
            response = urlopen(url).read()
            # parse the response from ipinfo.io
            geo_data = json.loads(response.decode('utf-8'))
            # extract the lat/lon pair
            self.latlon = [float(l) for l in geo_data['loc'].split(',')]
            
        
        if station_table is None:
            try:
                base_dir = os.path.dirname(os.path.abspath(__file__))
            except:
                base_dir = os.getcwd()
                
            station_table = "{}/ndbc_station_info.txt".format(base_dir)
            
        header = ["ID", "Hull", "LatDegrees", "LatMinutes", "LatSeconds", "LatHemisphere", "LonDegrees", "LonMinutes","LonSeconds", "LonHemisphere", "Depth", "Radius", "Type"]
        self.station_data = pd.read_table(station_table,skiprows=[0,1,2,3,4],delim_whitespace=True,error_bad_lines=False, warn_bad_lines=False, names=header)
        
        self.station_data['LatDegreesNorth'] = self.station_data['LatDegrees'] + self.station_data['LatMinutes']/60 + self.station_data['LatSeconds']/3600
        self.station_data['LonDegreesEast'] = self.station_data['LonDegrees'] + self.station_data['LonMinutes']/60 + self.station_data['LonSeconds']/3600
        
        # add +/- signs to the lat lon as appropriate
        iwest = self.station_data['LonHemisphere'] == 'W'
        self.station_data.loc[iwest,'LonDegreesEast'] *= -1
        isouth = self.station_data['LatHemisphere'] == 'S'
        self.station_data.loc[isouth,'LatDegreesNorth'] *= -1

        buoy_ids = list(self.station_data['ID'])
        buoy_lats = list(self.station_data['LatDegreesNorth'])
        buoy_lons = list(self.station_data['LonDegreesEast'])
        
        # calculate distances from the user location to each buoy
        self.distances = { id : distance_on_earth_sphere(blat,blon,self.latlon[0],self.latlon[1]) \
                                for id, blat, blon in zip(buoy_ids, buoy_lats, buoy_lons) \
                             }
        
    def get_buoys_sorted_by_distance(self):
        """ Returns a list of buoys sorted by distance. """
        
        sorted_buoys = sorted(self.distances, key=self.distances.__getitem__)
        return sorted_buoys
    
    def get_closest_buoy_id(self):
        """ Returns the ID of the closest buoy. """
        
        sorted_buoys = self.get_buoys_sorted_by_distance()
        
        return sorted_buoys[0]
        
        
